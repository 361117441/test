FROM registry.cn-chengdu.aliyuncs.com/xzlxzl/jre8
ADD target/test-0.0.1-SNAPSHOT.jar /app.jar
EXPOSE 8080
ENTRYPOINT ["java","-Xmx300m","-Dserver.port=8080","-jar","app.jar"]
